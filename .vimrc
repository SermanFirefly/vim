set noexpandtab
set shiftwidth=3
set tabstop=3
set autoindent
set pastetoggle=<F10>
set number
filetype plugin indent on
syntax on
syntax enable
inoremap jk <esc>
inoremap <esc> <nop>

